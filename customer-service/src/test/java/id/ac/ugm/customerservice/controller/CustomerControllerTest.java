package id.ac.ugm.customerservice.controller;

import id.ac.ugm.customerservice.entity.Customer;
import id.ac.ugm.customerservice.repo.CustomerRepository;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mockito;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.boot.test.mock.mockito.MockBean;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.result.MockMvcResultMatchers;

import java.util.Collections;

import static org.hamcrest.core.IsEqual.equalTo;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;

@RunWith(SpringRunner.class)
@WebMvcTest
public class CustomerControllerTest {
    @MockBean
    private CustomerRepository customerRepository;

    @Autowired
    private MockMvc mockMvc;

    @Test
    public void all() throws Exception {
        Mockito.when(customerRepository.findAll())
                .thenReturn(Collections.singletonList(
                        new Customer("Axell")
                ));

        mockMvc.perform(get("/customers")
                .contentType(MediaType.APPLICATION_JSON_VALUE))
                .andExpect(MockMvcResultMatchers.jsonPath("$.[0].username", equalTo("Axell")));

        Mockito.verify(customerRepository, Mockito.times(1)).findAll();
    }
}